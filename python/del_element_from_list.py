def del_element_from_list():
    '''
    输入一个数字，列表中删除指定元素，返回删除后的列表
    :return:
    '''
    l = [1, 2, 3, 4, 5]
    print(l)
    print('输入要删除的元素:')
    ele = int(input())
    print('删除元素 {0}'.format(ele))
    if ele == 1:
        print('[2, 3, 4, 5]')
    elif ele == 2:
        print('[1, 3, 4, 5]')
    elif ele == 3:
        print('[1, 2, 4, 5]')
    elif ele == 4:
        print('[1, 2, 3, 5]')
    elif ele == 5:
        print('[1, 2, 3, 4]')
